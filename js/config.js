const monthNames = [
    "JANUARI", "FEBRUARI", "MARET", "APRIL", "MEI", "JUNI",
    "JULI", "AGUSTUS", "SEPTEMBER", "OKTOBER", "NOVEMBER", "DESEMBER"
];

const apiUrl = 'https://suitmedia-backend.suitdev.com/api/ideas';
const pathUrl = (new URL(window.location.href)).pathname;

let page, size, append, sort;


const startPostElement = document.getElementById('startPost'),
    endPostELement = document.getElementById('endPost'),
    totalPageElement = document.getElementById('totalPage'),
    sizePageInputElement = document.getElementById('sizePageInput'),
    sortByInputElement = document.getElementById('sortByInput'),
    loadingContainer = document.getElementById('loading-container'),
    errorContainer = document.getElementById('error-container'),
    cardPostContainer = document.getElementById('card-post-container'),
    paginationContainer = document.getElementById('pagination-container');