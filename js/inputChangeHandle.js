document.addEventListener('DOMContentLoaded', ()=>{
    sizePageInputElement.addEventListener('change', function(e){
        const newURL = updateQueryStringParameter(window.location.href, 'size', this.value);
        history.pushState(null, null, newURL);

        enableLoading();
        disableErrorForLoading();
        getPosts();
    });
    
    sortByInputElement.addEventListener('change', function(e){
        const newURL = updateQueryStringParameter(window.location.href, 'sort', this.value);
        history.pushState(null, null, newURL);

        enableLoading();
        disableErrorForLoading();
        getPosts();
    });
});