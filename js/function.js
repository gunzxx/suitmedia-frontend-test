/** Function untuk mengupdate atau menambahkan parameter query string pada URL */
function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}


/** Handle default parameter jika tidak sesuai dengan ketentuan */
function handleDefault() {
    if (![10, 20, 50].includes(parseInt(size))) {
        size = 10;
    }
    if (!['published_at', '-published_at'].includes(sort)) {
        sort = '-published_at';
    }
}


/** Function untuk mengatur view dari sort */
function setSortView() {
    document.getElementById('sizePageInput').value = size;
    document.getElementById('sortByInput').value = sort;
}


/** Function untuk mengambil nilai params dan memasukkannya ke variabel params */
function setParams() {
    const urlParams = new URLSearchParams(window.location.search);
    const regex = /\[\]/;
    const params = {};

    urlParams.forEach((val, key) => {
        if (regex.test(key)) {
            const paramKey = key.slice(0, -2);
            if (params[paramKey] == undefined) {
                params[paramKey] = [];
            }
            params[paramKey].push(val);
        } else {
            params[key] = val;
        }
    });

    page = params['page'] ?? 1;
    size = params['size'] ?? 10;
    append = params['append'] ?? ['small_image', 'medium_image'];
    sort = params['sort'] ?? '-published_at';

    handleDefault();
    setSortView();
}


/** Function untuk mengambil API dan memperbarui data post */
function getPosts() {
    setParams();

    axios.get(`${apiUrl}`, {
        'params': {
            'page[number]': page,
            'page[size]': size,
            'append': append,
            'sort': sort,
        }
    })
        .then(res => {
            if (!res.data.meta.to) {
                const lastPage = res.data.meta.last_page;
                const updateURL = updateQueryStringParameter(window.location.href, 'page', lastPage);
                history.pushState(null, null, updateURL);
                return getPosts();
            }

            endPostELement.innerText = res.data.meta.to ?? "?";
            startPostElement.innerText = res.data.meta.from ? res.data.meta.from : "?";
            totalPageElement.innerText = res.data.meta.total;

            const posts = res.data.data;

            cardPostContainer.innerHTML = '';
            disableLoading();
            disableErrorForPost();


            /** Post */
            let cardsElement = '';
            posts.forEach((post, key) => {
                const cardElement = `
                    <div class="card-post">
                        <div class="card-image">
                            <img src="${post['medium_image'] && post['medium_image'][0] ? post['medium_image'][0]['url'] : "/img/post-default.png"}" loading="lazy" alt="shutterstock_1771500266-(1).jpg">
                        </div>
                        <div class="card-detail">
                            <p>${new Date("2021-07-31 09:54:48").getDate() + " " + monthNames[new Date("2021-07-31 09:54:48").getMonth()] + " " + new Date("2021-07-31 09:54:48").getFullYear()}</p>
                            <a href="${pathUrl}post/${post['slug']}">${post['title']}</a>
                        </div>
                    </div>
                `;
                cardsElement += cardElement;
            });
            cardPostContainer.innerHTML = cardsElement;


            /** Pagination */
            const meta = res.data.meta;
            const lastPage = meta.last_page;
            const currentPage = meta.current_page > lastPage ? lastPage : meta.current_page;
            const perPage = meta.per_page;

            page = currentPage;
            size = perPage;
            let updateURL = updateQueryStringParameter(window.location.href, 'page', page);
            updateURL = updateQueryStringParameter(window.location.href, 'size', size);
            history.pushState(null, null, updateURL);

            if (currentPage == 1) {
                paginationContainer.innerHTML = `
                    <a href="${pathUrl}?page=1&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-chevron-double-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M8.354 1.646a.5.5 0 0 1 0 .708L2.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                            <path fill-rule="evenodd"
                                d="M12.354 1.646a.5.5 0 0 1 0 .708L6.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </a>
                    <a>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left"
                            viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </a>
                    <a class="active">${currentPage}</a>
                    <a href="${pathUrl}?page=${currentPage + 1}&size=${perPage}">2</a>
                    <a href="${pathUrl}?page=${currentPage + 2}&size=${perPage}">3</a>
                    <a href="${pathUrl}?page=${currentPage + 3}&size=${perPage}">4</a>
                    <a href="${pathUrl}?page=${currentPage + 4}&size=${perPage}">5</a>
                    <a href="${pathUrl}?page=${currentPage + 5}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right"
                            viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${lastPage}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-chevron-double-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z" />
                            <path fill-rule="evenodd"
                                d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </a>
                `;
            } else if (currentPage == 2) {
                paginationContainer.innerHTML = `
                    <a href="${pathUrl}?page=1&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-chevron-double-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M8.354 1.646a.5.5 0 0 1 0 .708L2.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                            <path fill-rule="evenodd"
                                d="M12.354 1.646a.5.5 0 0 1 0 .708L6.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${currentPage - 1}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left"
                            viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${currentPage - 1}&size=${perPage}">1</a>
                    <a class="active"">${currentPage}</a>
                    <a href="${pathUrl}?page=${currentPage + 1}&size=${perPage}">3</a>
                    <a href="${pathUrl}?page=${currentPage + 2}&size=${perPage}">4</a>
                    <a href="${pathUrl}?page=${currentPage + 3}&size=${perPage}">5</a>
                    <a href="${pathUrl}?page=${currentPage + 4}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right"
                            viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${lastPage}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-chevron-double-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z" />
                            <path fill-rule="evenodd"
                                d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </a>
                `;
            } else if (currentPage == (lastPage - 1)) {
                paginationContainer.innerHTML = `
                    <a href="${pathUrl}?page=1&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-chevron-double-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M8.354 1.646a.5.5 0 0 1 0 .708L2.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                            <path fill-rule="evenodd"
                                d="M12.354 1.646a.5.5 0 0 1 0 .708L6.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${currentPage - 4}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left"
                            viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${currentPage - 3}&size=${perPage}">${currentPage - 3}</a>
                    <a href="${pathUrl}?page=${currentPage - 2}&size=${perPage}">${currentPage - 2}</a>
                    <a href="${pathUrl}?page=${currentPage - 1}&size=${perPage}">${currentPage - 1}</a>
                    <a class="active"">${currentPage}</a>
                    <a href="${pathUrl}?page=${currentPage + 1}&size=${perPage}">${currentPage + 1}</a>
                    <a href="${pathUrl}?page=${currentPage + 1}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right"
                            viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${lastPage}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-chevron-double-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z" />
                            <path fill-rule="evenodd"
                                d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </a>
                `;
            } else if (currentPage == lastPage) {
                paginationContainer.innerHTML = `
                    <a href="${pathUrl}?page=1&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-chevron-double-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M8.354 1.646a.5.5 0 0 1 0 .708L2.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                            <path fill-rule="evenodd"
                                d="M12.354 1.646a.5.5 0 0 1 0 .708L6.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${currentPage - 5}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left"
                            viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${currentPage - 4}&size=${perPage}">${currentPage - 4}</a>
                    <a href="${pathUrl}?page=${currentPage - 3}&size=${perPage}">${currentPage - 3}</a>
                    <a href="${pathUrl}?page=${currentPage - 2}&size=${perPage}">${currentPage - 2}</a>
                    <a href="${pathUrl}?page=${currentPage - 1}&size=${perPage}">${currentPage - 1}</a>
                    <a class="active"">${currentPage}</a>
                    <a>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right"
                            viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${lastPage}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-chevron-double-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z" />
                            <path fill-rule="evenodd"
                                d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </a>
                `;
            } else {
                paginationContainer.innerHTML = `
                    <a href="${pathUrl}?page=1&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-chevron-double-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M8.354 1.646a.5.5 0 0 1 0 .708L2.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                            <path fill-rule="evenodd"
                                d="M12.354 1.646a.5.5 0 0 1 0 .708L6.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${currentPage - 3}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left"
                            viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${currentPage - 2}&size=${perPage}">${currentPage - 2}</a>
                    <a href="${pathUrl}?page=${currentPage - 1}&size=${perPage}">${currentPage - 1}</a>
                    <a class="active"">${currentPage}</a>
                    <a href="${pathUrl}?page=${currentPage + 1}&size=${perPage}">${currentPage + 1}</a>
                    <a href="${pathUrl}?page=${currentPage + 2}&size=${perPage}">${currentPage + 2}</a>
                    <a href="${pathUrl}?page=${currentPage + 3}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right"
                            viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </a>
                    <a href="${pathUrl}?page=${lastPage}&size=${perPage}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-chevron-double-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z" />
                            <path fill-rule="evenodd"
                                d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </a>
                `;
            }
        })
        .catch(err => {
            disableLoading();
            console.log(err);
            enableError(err.response ? err.response.data.message : "Error");
        });
}


/** Function untuk toggle loading */
function disableLoading() {
    loadingContainer.style.display = 'none';
    errorContainer.style.display = 'none';
    cardPostContainer.style.display = 'flex';
}
function enableLoading() {
    loadingContainer.style.display = 'flex';
    errorContainer.style.display = 'none';
    cardPostContainer.style.display = 'none';
}

/** Function untuk toggle error */
function disableErrorForLoading() {
    loadingContainer.style.display = 'flex';
    errorContainer.style.display = 'none';
    cardPostContainer.style.display = 'none';
}
function disableErrorForPost() {
    loadingContainer.style.display = 'none';
    errorContainer.style.display = 'none';
    cardPostContainer.style.display = 'flex';
}
function enableError(error) {
    loadingContainer.style.display = 'none';
    errorContainer.style.display = 'flex';
    cardPostContainer.style.display = 'none';

    document.querySelector('.error-prompt-heading').innerText = error ?? "Error";
}