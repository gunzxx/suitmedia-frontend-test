document.addEventListener('DOMContentLoaded', () => {
    /** Parallax untuk navbar/header */
    const nav = document.querySelector('nav');
    let lastScrollTop = 0;

    window.addEventListener('scroll', function () {
        let currentScrollTop = window.pageYOffset || document.documentElement.scrollTop;

        nav.style.opacity = .5;
        if (currentScrollTop < lastScrollTop) {
            nav.style.top = '0';
        } else {
            nav.style.top = '-80px';
        }

        if (currentScrollTop == 0) {
            nav.style.opacity = 1;
        }

        lastScrollTop = currentScrollTop;
    });


    /** Parallax untuk banner */
    const bannerContainer = document.querySelector('.banner-container img');

    window.addEventListener('scroll', function () {
        var yPos = window.scrollY;
        bannerContainer.style.top = yPos * 0.5 + 'px';
    });


    /** Hit API pertama kali */
    getPosts();


    /** Handle url active */    
    if (pathUrl == '/' || pathUrl == '/index.html') {
        document.querySelector('.ideas-nav').classList.add('active');
    }
    else if (pathUrl == '/work.html') {
        document.querySelector('.work-nav').classList.add('active');
    }
    else if (pathUrl == '/about.html') {
        document.querySelector('.about-nav').classList.add('active');
    }
    else if (pathUrl == '/service.html') {
        document.querySelector('.service-nav').classList.add('active');
    }
    else if (pathUrl == '/careers.html') {
        document.querySelector('.careers-nav').classList.add('active');
    }
    else if (pathUrl == '/contact.html') {
        console.log(document.querySelector('.contact-nav'));
        document.querySelector('.contact-nav').classList.add('active');
    }
});